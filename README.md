Jilia Sandbox Server
====================

This project is useful for creating a Jilia sandbox containing mock devices.  This can be used to create a simple test environment or it can be used to model devices that are not yet released for pre-testing.

# Installation

Install with `npm install -g git+https://bitbucket.org/jilia/jilia-sandbox-server.git`

# Usage
## Standalone Server
To spin up a single instance, run `node app.js [options]`.  The only mandatory option is `--auth` which expects a Hub app client/secret token in the form `clientKey:clientSecret`.

Example:

        jilia-sandbox --auth 'aLFKJ4982qlkj12:198uklajflajdf'

Though no devices will be created with the above command.  See the various command line switch to have the server create mock devices.

### Including Custom Devices

Custom devices can be included with the `--include` option.  It expects a list of devices (the path to the node_module) separated by spaces.  If the device expects parameters, the can be included with the device name separated by commas

Example:

        jilia-sandbox --auth 'keys' --include ./foo,bar biz,baz,boom

This would "require()" the device "./foo" (relative to the current path) and pass in `bar` as the only parameter.  It would also "require()" the device "biz" from the "node_modules" directory and pass in `baz` and `boom`.

## Multiple Instances
If you need to spawn multiple instaces of the sandbox server, you may use the `spawn` utility.

Example:

        jilia-sandbox -c 10 --auth 'keys' --thermostats 1

The above example would spawn 10 instances with 1 thermostat each.  Each instance is staretd in a new process so if it crashes the app will keep trucking along.

## Persistence
The above examples will start a server with a random UUID for every invocation.  Devices within the server will also have different UUIDs upon each invokation.  If you would like to have a persistent server for testing (or set of servers when using '--count'), then you can use the `--persistence` flag.

Example (Single Instance):

        jilia-sandbox --auth 'keys' --name TestServer1 --persistence

This will start the server with the name "TestServer1" (as apposed to jilia-sandbox-<uuid>) and save the persistence information in the *current working directory*.  When launching a single instance with `--persistence`, `--name <name>` is *mandatory*.

Example (Multiple Instances):

        jilia-sandbox --auth 'keys' --persistence --count 5

This will create 5 instances of the sandbox.  If the program is closed and rerun, all the servers will retain their name from the previous invocation.  Devices created inside each server will also maintain their UUID.