#!/usr/bin/env node
var Thermostat = require('jilia-mock-thermostat'),
    Motion = require('jilia-mock-motion-sensor'),
    Door = require('jilia-mock-door-sensor'),
    Relay = require('jilia-mock-relay'),
    Dimmer = require('jilia-mock-dimmer'),
    Sandbox = require('jilia-sandbox');

var argv = require('yargs')
    .usage('Usage: $0 [options]')
    .example('$0 -i ./foo,bar,baz ./test,bar,baz','Will "require()" ./foo and ./test and pass bar & baz as parameters')
    .option('auth', {
      alias : 'a',
      demand : false,
      describe : 'Credentials as \'clientKey:clientSecret\''
    })
    .option('thermostats', {
      describe:'Number of thermostat devices to include',
    })
    .option('motions', {
      describe:'Number of motion devices to include',
    })
    .option('doors', {
      describe:'Number of door devices to include',
    })
    .option('relays', {
      describe:'Number of relay devices to include',
    })
    .option('dimmers', {
      describe:'Number of dimmers devices to include',
    })
    .option('port', {
      alias: 'p',
      describe:'Specify a port',
      default: 0
    })
    .option('url', {
      describe: 'Alternate base URL to use'
    })
    .option('include', {
      alias: 'i',
      describe: 'device modules to include comma delimited with parameters',
      array: 'includes'
    })
    .option('name', {
      alias: 'n',
      describe: 'Set the server name'
    })
    .option('persistence', {
      describe: 'Enable persistence (requires count > 1 || name != null)',
      default : false
    })
    .option('c',{
      alias: 'count',
      describe:'Number of instances to spawn',
      type:'number',
      default: 1
    })
    .option('output',{
      describe: '(if count > 1) STDOUT is disabled by default, this enables it',
      default: false
    })
    .option('delay',{
      alias: 'd',
      describe: '(if count > 1) Delay between starting each sandbox in MS',
      default: 1000
    })
    .strict()
    .argv;

var devices = []

if(argv.thermostats) {
  for(var i=0;i<argv.thermostats;++i) {
    devices.push([Thermostat,'Thermostat' + (i+1)]);
  }
}

if(argv.motions) {
  for(var i=0;i<argv.motions;++i) {
    devices.push([Motion,'Motion' + (i+1)]);
  }
}

if(argv.doors) {
  for(var i=0;i<argv.doors;++i) {
    devices.push([Door,'Door' + (i+1)]);
  }
}

if(argv.relays) {
  for(var i=0;i<argv.relays;++i) {
    devices.push([Relay,'Relay' + (i+1)]);
  }
}

if(argv.dimmers) {
  for(var i=0;i<argv.dimmers;++i) {
    devices.push([Dimmer,'Dimmer' + (i+1)]);
  }
}

if(argv.include) {
  argv.include.forEach(function(i) {
    var args = i.split(',');
    // replace the name of the module with the module's constructor
    args[0] = require(args[0]);
    devices.push(args);
  })
}

if(argv.count > 1) {
  // get rid of our 'count' switch before we spawn sub-processes
  var idx = process.argv.indexOf('--count');
  if(idx === -1) {
    idx = process.argv.indexOf('-c');
  }
  process.argv.splice(idx, 2);

  var forever = require('forever-monitor');
  var i = argv.count;

  function spawn() {
    // copy the array
    var args = process.argv.slice();
    var num = (argv.count - i + 1);

    if(argv.persistence) {
      var idx = args.indexOf('--persistence');
      args.splice(idx + 1, 0, num);
    }

    var child = new (forever.Monitor)(__filename,{
      silent: !argv.output,
      minUpTime: 5000,
      spinSleepTime: 5000,
      args : args
    });

    console.log('Spawning sandbox #' + num);
    child.start();

    i--;
    if(i > 0 ) {
      setTimeout(spawn, argv.delay);
    }
  }

  spawn();
} else {
  var persistenceId = null;
  if(argv.persistence === true) {
    if(argv.name) {
      persistenceId = argv.name;
    } else {
      throw new Error('--persistence requires --name');
    }
  } else if(argv.persistence) {
    persistenceId = argv.persistence.toString();
  }

  var sbxOptions = {
    name: argv.name,
    persistenceId : persistenceId
  };

  if(argv.auth) {
    sbxOptions.link = {
      url: argv.url,
      credentials : argv.auth,
    }
  }

  var sandbox = new Sandbox(devices, sbxOptions);

  sandbox.listen(argv.port);
}